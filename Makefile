
all: manipulation_csv.html manipulation_csv.pdf normalisation.pdf

manipulation_csv.pdf: manipulation_csv.md
	pandoc -o $@ $<

manipulation_csv.html: manipulation_csv.md
	pandoc -s -o $@ $< 

normalisation.pdf: normalisation.tex
	pdflatex normalisation.tex

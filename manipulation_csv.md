# Feuille de triche sur la manipulation de CSV en ligne de commande

## Selection d'un sous-ensemble de colonne

### Avec `awk`

Commande:

```
awk '{FS=";"; OFS=";"} {print $1,$2,$4}' input.csv > output.csv
```

Avec:

 - `FS` le séparateur de colonne du fichier entrée
 - `OFS` le séparateur de colonne du fichier sortie
 - `$1,$2,$4` les colonnes qu'on veut conserver (ici la première, la seconde et la
   quatrième).

### Avec `cut`

Commande:

```
cut -d ';' -f 1-2,4 input.csv > output.csv
```

Avec:

 - `-d ';'` le séparateur de colonne du fichier entrée et du fichier sortie
 - `-f 1-2,4` les colonnes qu'on veut conserver (ici toutes les colonnes de la
   première à la seconde et la quatrième).

## Rajouter une colonne numéro de ligne pour faire une AK

Commande:

```
awk '{FS=";"; OFS=";"} {print NR,$1,$2,$4}' input.csv > output.csv
```

Avec:

 - `FS` le séparateur de colonne du fichier entrée
 - `OFS` le séparateur de colonne du fichier sortie
 - `NR` la variable de `awk` qui contient le numéro des lignes
 - `$1,$2,$4` les colonnes qu'on veut conserver (ici la première, la seconde et la
   quatrième`.

## Gestion des doublons

### Enlever les lignes dont des colonnes sont dupliquées

Commande:

La commande trie l'entrée par rapport à une clef, et supprime les doublons sur
la clefs.

```
sort -t ';' -k 3,5 -u input.csv > output.csv
```

Avec:

 - `-t ';'` le séparateur de colonne utilisé pour la construction de la clef
 - `-k 3,5` la clef utilisée pour le tri et la suppression des doublons (ici les
   colonnes 3 à 5.

Attention pour utiliser qu'une colonne en clef de tri et de
suppression, il faut la préciser en double, par exemple pour utiliser la colonne
3, il faudra indiquer `-k 3,3`.
